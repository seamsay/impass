use clap::{crate_authors, crate_description, crate_name, crate_version};
use std::io::Write;


fn main() -> anyhow::Result<()> {
    let matches = clap::app_from_crate!()
        .help_message("Print help information.")
        .version_message("Print version information.")
        .arg(
            clap::Arg::with_name("password")
                .long("password")
                .short("p")
                .help("The password with which to decrypt the database.")
                .long_help(
                    "The password with which to decrypt the database.

If this option is not given you will be prompted for the password either using the program specified by `$SSH_ASKPASS` or the TTY.

WARNING: Take care not to accidentally leak you password when using this option.",
                )
                .takes_value(true),
        )
        .group(
            clap::ArgGroup::with_name("database")
                .args(&["database.file", "database.ssh"])
                .required(true),
        )
        .arg(
            clap::Arg::with_name("database.file")
                .long("database.file")
                .short("f")
                .help("Load the database from the given file.")
                .long_help(
                    "The path to the database.

If the file does not exist a new database will be created and saved to this file.",
                )
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("database.ssh")
                .long("database.ssh")
                .short("s")
                .help("Load the database from the given SSH server.")
                .long_help(
                    "Load the database from the given SSH server.

The value of this argument should be of the form `<username>@<url>:<path to file>`, as it would be for `scp`.",
                )
                .takes_value(true),
        )
        .subcommand(
            clap::SubCommand::with_name("group")
                .about("View a group in the database.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .arg(
                    clap::Arg::with_name("path")
                        .help("Path to the group to view.")
                        .min_values(0),
                )
                .arg(
                    clap::Arg::with_name("recursive")
                        .long("recursive")
                        .short("r")
                        .help("Print children of the given group as well."),
                ),
        )
        .subcommand(
            clap::SubCommand::with_name("entry")
                .about("View a entry in the database.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .group(
                    clap::ArgGroup::with_name("entry")
                        .args(&["path", "id"])
                )
                .arg(
                    clap::Arg::with_name("path")
                        .long("path")
                        .short("p")
                        .help("Path to the group that contains the entry.")
                        .takes_value(true)
                        .min_values(0)
                        .requires("name"),
                )
                .arg(
                    // TODO: Validate that this is an integer.
                    clap::Arg::with_name("id")
                        .long("id")
                        .short("i")
                        .help("ID of the entry to view.")
                        .takes_value(true)
                        .conflicts_with("name"),
                )
                .arg(
                    clap::Arg::with_name("name")
                        .long("name")
                        .short("n")
                        .help("Name of the entry to view.")
                        .takes_value(true),
                )
                .group(
                    clap::ArgGroup::with_name("show")
                        .args(&["mime", "hints", "value"])
                        .requires("field"),
                )
                .arg(
                    clap::Arg::with_name("field")
                        .long("field")
                        .short("f")
                        .help("The specific field to show.")
                        .takes_value(true),
                )
                .arg(
                    clap::Arg::with_name("mime")
                        .long("mime")
                        .short("m")
                        .help("Show the MIME type of this field."),
                )
                .arg(
                    clap::Arg::with_name("hints")
                        .long("hints")
                        .short("h")
                        .help("Show the hints that this field has."),
                )
                .arg(
                    clap::Arg::with_name("value")
                        .long("value")
                        .short("v")
                        .help("Show the value of this field.")
                        .long_help(
                            "Show the value of this field.

This will print the value verbatim even if it is a binary field."
                        ),
                ),
        )
        .subcommand(
            clap::SubCommand::with_name("store")
                .about("View the contents of the store.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .arg(
                    clap::Arg::with_name("leaves")
                        .long("leaves")
                        .short("l")
                        .help("Only print the leaves of the store."),
                ),
        )
        .subcommand(
            clap::SubCommand::with_name("add")
                .about("Add a new entry to the database.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .arg(
                    clap::Arg::with_name("field")
                        .long("field")
                        .short("f")
                        .help("Add a field to this entry.")
                        .long_help(
                            "Add a field to this entry.

This argument can be specified multiple times, with each occurrence adding a seperate field.

This argument requires 2 or more values. The first value is the name of the field, the second value is the value of this field, the third value is the MIME type of the field, and the remaining parameters are a list of hints. If the MIME type is not present or an empty string then the `text/plain; charset=utf-8` MIME type will be used.

The value of this field _must_ be a valid UTF-8 string. If it is not then the program will crash, albeit safely. The `file` argument can be used to enter non-UTF-8 values.",
                        )
                        .takes_value(true)
                        .multiple(true)
                        .min_values(2),
                )
                .arg(
                    clap::Arg::with_name("file")
                        .long("file")
                        .short("b")
                        .help("Add a field to this entry, with the value read from a file.")
                        .long_help(
                            "Add a field to this entry, with the value read from a file.

This argument can be specified multiple times, with each occurrence adding a seperate field.

This argument requires 2 or more values. The first value is the name of the field, the second value is a file containing the value of this field, the third value is the MIME type of the field, and the remaining parameters are a list of hints. If the MIME type is not present or an empty string then the the `application/octet-stream` MIME type will be used.",
                        )
                        .takes_value(true)
                        .multiple(true)
                        .min_values(2),
                )
                .arg(
                    clap::Arg::with_name("path")
                        .long("path")
                        .short("p")
                        .help("Path to this entry.")
                        .long_help(
                            "Path to this entry.

The last value given to this argument is the name of the entry itself and is required.

This argument can be specified multiple times, with each occurrence adding to a separate group."
                        )
                        .takes_value(true)
                        .multiple(true)
                        .min_values(1),
                )
                .arg(
                    clap::Arg::with_name("create")
                        .long("create")
                        .short("c")
                        .help("Create any groups that don't exist."),
                ),
        )
        .subcommand(
            clap::SubCommand::with_name("update")
                .about("Update an entry in the database.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .arg(
                    clap::Arg::with_name("all")
                        .long("all")
                        .short("a")
                        .help("Update all occurrences of this entry in the hierarchy."),
                )
                .group(
                    clap::ArgGroup::with_name("entry")
                        .args(&["id", "path"])
                        .required(true),
                )
                .arg(
                    // TODO: Validate that this is a number.
                    clap::Arg::with_name("id")
                        .long("id")
                        .help("The ID of the entry to update.")
                        .long_help(
                            "The ID of the entry to update.

If `--all` is not present this will not affect the hierarchy at all.",
                        )
                        .takes_value(true),
                )
                .arg(
                    clap::Arg::with_name("path")
                        .long("path")
                        .short("p")
                        .help("The path in the hierarchy to the entry to update.")
                        .long_help(
                            "The path in the hierarchy to the entry to update.

If `--all` is not present this will only update the given path, otherwise it will update all paths with that ID.",
                        )
                        .takes_value(true)
                        .min_values(1),
                )
                .arg(
                    clap::Arg::with_name("field")
                        .long("field")
                        .short("f")
                        .help("Insert a new field into the entry.")
                        .long_help(
                            "Insert a new field into this entry.

This argument can be specified multiple times, with each occurrence adding a seperate field.

This argument requires 2 or more values. The first value is the name of the field, the second value is the value of this field, the third value is the MIME type of the field, and the remaining parameters are a list of hints. If the MIME type is not present or an empty string then the `text/plain; charset=utf-8` MIME type will be used.

The value of this field _must_ be a valid UTF-8 string. If it is not then the program will crash, albeit safely. The `file` argument can be used to enter non-UTF-8 values.",
                        )
                        .multiple(true)
                        .min_values(2),
                )
                .arg(
                    clap::Arg::with_name("file")
                        .long("file")
                        .short("b")
                        .help("Insert a new field into the entry, with the value read from a file.")
                        .long_help(
                            "Insert a new field into this entry, with the value read from a file.

This argument can be specified multiple times, with each occurrence adding a seperate field.

This argument requires 2 or more values. The first value is the name of the field, the second value is a file containing the value of this field, the third value is the MIME type of the field, and the remaining parameters are a list of hints. If the MIME type is not present or an empty string then the the `application/octet-stream` MIME type will be used.",
                        )
                        .multiple(true)
                        .min_values(2),
                )
                .arg(
                    clap::Arg::with_name("remove")
                        .long("remove")
                        .short("r")
                        .help("Remove the given field from this entry.")
                        .long_help(
                            "Remove the given field from this entry.

This argument can be specified multiple times with each occurrence removing a different field.",
                        )
                        .takes_value(true)
                        .multiple(true)
                        .number_of_values(1),
                ),
        )
        .subcommand(
            clap::SubCommand::with_name("generate")
                .about("Generate a random password.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .subcommand(
                    clap::SubCommand::with_name("random")
                        .about("Generate a random password with the given characteristics.")
                        .help_message("Print help information.")
                        .version_message("Print version information.")
                        .arg(
                            // TODO: Validate that this is a number.
                            clap::Arg::with_name("length")
                                .long("length")
                                .short("l")
                                .help("The length of the generated password.")
                                .takes_value(true)
                                .default_value("32"),
                        )
                        .group(
                            clap::ArgGroup::with_name("characteristic")
                                .args(&["alphabet", "number", "symbols"])
                                .required(true)
                                .conflicts_with("bytes")
                                .multiple(true),
                        )
                        .arg(
                            clap::Arg::with_name("alphabet")
                                .long("alphabet")
                                .short("a")
                                .help("Use characters from the latin alphabet in the password."),
                        )
                        .arg(
                            clap::Arg::with_name("number")
                                .long("number")
                                .short("n")
                                .help("Use numbers in the password."),
                        )
                        .arg(
                            clap::Arg::with_name("symbols")
                                .long("symbols")
                                .short("s")
                                .help("Use ASCII symbols in the password.")
                                .long_help(
                                    "Use ASCII symbols in the password.

This does not include whitespace or characters that are not printable.",
                                ),
                        )
                        .arg(
                            clap::Arg::with_name("bytes")
                                .long("bytes")
                                .short("b")
                                .help("Use any byte in the password.")
                                .long_help(
                                    "Use any byte in the password.

The password will be printed as a hex string if this option is given.",
                                ),
                        ),
                )
                .subcommand(
                    clap::SubCommand::with_name("pattern")
                        .about("Generate a password that matches the given pattern.")
                        .help_message("Print help information.")
                        .version_message("Print version information."),
                )
                .setting(clap::AppSettings::SubcommandRequired)
        )
        .subcommand(
            clap::SubCommand::with_name("hierarchy")
                .about("Change the hierarchy of the database.")
                .help_message("Print help information.")
                .version_message("Print version information.")
                .subcommand(
                    clap::SubCommand::with_name("add")
                        .about("Add a group or entry to the hierarchy.")
                        .help_message("Print help information.")
                        .version_message("Print version information.")
                        .group(
                            clap::ArgGroup::with_name("add")
                                .args(&["id", "path", "name"])
                                .required(true),
                        )
                        .arg(
                            // TODO: Validate that this is an interger.
                            clap::Arg::with_name("id")
                                .long("id")
                                .short("i")
                                .help("Add an entry with the given ID to the group.")
                                .takes_value(true),
                        )
                        .arg(
                            clap::Arg::with_name("path")
                                .long("path")
                                .short("p")
                                .help("Add an entry with the same ID as the one given.")
                                .takes_value(true)
                                .min_values(1),
                        )
                        .arg(
                            clap::Arg::with_name("name")
                                .long("name")
                                .short("n")
                                .help("The name of the group or entry.")
                                .takes_value(true)
                                .required(true),
                        )
                        .arg(
                            clap::Arg::with_name("group")
                                .long("group")
                                .short("g")
                                .help("Group to which the group or entry is being added.")
                                .takes_value(true)
                                .multiple(true)
                                .min_values(0)
                                .required(true),
                        ),
                )
                .subcommand(
                    clap::SubCommand::with_name("remove")
                        .about("Remove a group or entry from the hierarchy.")
                        .help_message("Print help information.")
                        .version_message("Print version information.")
                        .arg(
                            clap::Arg::with_name("recursive")
                                .long("recursive")
                                .short("r")
                                .help("When removing a group, remove all child groups."),
                        )
                        .arg(
                            clap::Arg::with_name("path")
                                .long("path")
                                .short("p")
                                .help("Path to the group from which the group or entry will be removed.")
                                .takes_value(true)
                                .min_values(0),
                        )
                        .group(
                            clap::ArgGroup::with_name("name")
                                .args(&["entry", "group"])
                                .required(true),
                        )
                        .arg(
                            clap::Arg::with_name("entry")
                                .long("entry")
                                .short("e")
                                .help("Remove an entry with the given name from the group.")
                                .takes_value(true),
                        )
                        .arg(
                            clap::Arg::with_name("group")
                                .long("group")
                                .short("g")
                                .help("Remove a group with the given name.")
                                .takes_value(true),
                        )
                )
                .setting(clap::AppSettings::SubcommandRequired)
        )
        .setting(clap::AppSettings::SubcommandRequired)
        .get_matches();

    fn ask<P: AsRef<std::ffi::OsStr>>(program: P) -> anyhow::Result<String> {
        let output = std::process::Command::new(program).output()?;
        if output.status.success() {
            String::from_utf8(output.stdout).map_err(anyhow::Error::from)
        } else {
            Err(anyhow::anyhow!(
                "Failed to ask for password: {}",
                String::from_utf8_lossy(&output.stderr)
            ))
        }
    }

    let password = if let Some(password) = matches.value_of("password") {
        password.into()
    } else {
        match std::env::var("SSH_ASKPASS") {
            Ok(program) => ask(program)?,
            Err(std::env::VarError::NotUnicode(program)) => ask(program)?,
            Err(std::env::VarError::NotPresent) => {
                rpassword::read_password_from_tty(Some("Database Password: "))?
            }
        }
    };
    let password = password.as_bytes();

    let mut database = if let Some(path) = matches.value_of("database.file") {
        let path = std::path::Path::new(path);
        if path.exists() {
            let mut file = std::fs::File::open(path)?;
            impass::Encrypted::load(&mut file)?.decrypt(password)?
        } else {
            impass::encrypted::Decrypted {
                metadata: impass::PersistentMetadata::default(),
                database: impass::Database::default(),
            }
        }
    } else if let Some(destination) = matches.value_of("database.ssh") {
        let _ = destination;
        todo!()
    } else {
        unreachable!()
    };

    #[allow(unused_variables)]
    match matches.subcommand() {
        ("group", Some(matches)) => group(&database.database, &matches)?,
        ("entry", Some(matches)) => entry(&database.database, &matches)?,
        ("add", Some(matches)) => add(&mut database.database, &matches)?,
        ("update", Some(matches)) => todo!(),
        ("generate", Some(matches)) => todo!(),
        ("hierarchy", Some(matches)) => todo!(),
        _ => unreachable!(),
    }

    if let Some(path) = matches.value_of("database.file") {
        let path = std::path::Path::new(path);
        database
            .encrypt(password)
            .map_err(anyhow::Error::from)
            .and_then(|database| {
                std::fs::OpenOptions::new()
                    .create(true)
                    .write(true)
                    .truncate(true)
                    .open(path)
                    .map_err(anyhow::Error::from)
                    .and_then(|file| database.save(file).map_err(anyhow::Error::from))
            })
    } else if let Some(destination) = matches.value_of("database.ssh") {
        let _ = destination;
        todo!()
    } else {
        unreachable!()
    }
}


fn group(database: &impass::Database, matches: &clap::ArgMatches) -> anyhow::Result<()> {
    // TODO: Use dimensions to wrap text appropriately.
    fn print(
        group: &impass::database::Group,
        store: &[impass::database::Entry],
        level: usize,
        recurse: bool,
        dimensions: (usize, usize),
    ) -> anyhow::Result<()> {
        let indent = " ".repeat(level);

        println!("{}Entries:", indent);

        for (name, id, entry) in {
            let mut entries = group
                .entries()
                .iter()
                .map(|(name, &id)| (name, id, &store[id]))
                .collect::<Vec<_>>();
            // TODO: Can we get rid of this allocation?
            entries.sort_unstable_by_key(|entry| entry.0.to_string());
            entries
        } {
            println!("{} {:05} - {}", indent, id, name);

            for (name, field) in {
                let mut fields = entry.fields().iter().collect::<Vec<_>>();
                fields.sort_unstable_by_key(|(name, _)| *name);
                fields
            } {
                println!(
                    "{}  {} ({}) - {}",
                    indent,
                    name,
                    field.mime().essence_str(),
                    match field.value()? {
                        impass::database::FieldValue::Utf8(value) => value,
                        impass::database::FieldValue::Binary(_) => "<binary string>",
                    }
                )
            }
        }

        println!("{}Groups:", indent);

        for (name, group) in {
            let mut groups = group.groups().iter().collect::<Vec<_>>();
            groups.sort_unstable_by_key(|(name, _)| *name);
            groups
        } {
            print!("{} {}", indent, name);
            if recurse {
                println!(":");
                print(group, store, level + 2, recurse, dimensions)?;
            } else {
                println!();
            }
        }

        Ok(())
    }

    let path = matches
        .values_of("path")
        .map(|values| values.collect::<Vec<_>>())
        .unwrap_or_else(|| vec![]);

    let group = database
        .hierarchy
        .at(&path)
        .ok_or_else(|| impass::database::GroupDoesNotExist::from(&path[..]))?;

    print(
        group,
        database.entries(),
        0,
        matches.is_present("recursive"),
        // If stdout is not a TTY then assume we can use any width.
        termize::dimensions_stdout().unwrap_or((usize::max_value(), usize::max_value())),
    )?;

    Ok(())
}


fn entry(database: &impass::Database, matches: &clap::ArgMatches) -> anyhow::Result<()> {
    let id = if let Some(id) = matches.value_of("id") {
        id.parse()?
    } else if let Some(path) = matches.values_of("path") {
        let path = path.collect::<Vec<_>>();

        let group = database
            .hierarchy
            .at(&path)
            .ok_or_else(|| impass::database::GroupDoesNotExist::from(&path[..]))?;

        let name = matches.value_of("name").expect("`path` requires `name`");

        group
            .entries()
            .iter()
            .map(|(name, &id)| (name, id))
            .find(|(this_name, _)| this_name == &name)
            .ok_or_else(|| {
                anyhow::anyhow!("No entry called `{}` in group at path `{:?}`.", name, path)
            })?
            .1
    } else {
        unreachable!()
    };

    let entry = &database.entries()[id];

    if let Some(field) = matches.value_of("field") {
        let field = entry
            .fields()
            .iter()
            .find(|(name, _)| name == &field)
            .ok_or_else(|| anyhow::anyhow!("No field called `{}` in entry `{:05}`.", field, id))?
            .1;

        let all = matches.occurrences_of("show") == 0;

        if all || matches.is_present("mime") {
            let mime = field.mime();

            print!("{}/{}", mime.type_(), mime.subtype());

            if let Some(suffix) = mime.suffix() {
                print!("+{}", suffix);
            }

            for (key, value) in mime.params() {
                print!("; {}={}", key, value);
            }

            println!();
        }

        if all || matches.is_present("hints") {
            println!(
                "{}",
                field
                    .hints()
                    .iter()
                    .map(|hint| format!("{:?}", hint))
                    .collect::<Vec<_>>()
                    .join(" ")
            );
        }

        if all || matches.is_present("value") {
            match field.value()? {
                impass::database::FieldValue::Utf8(value) => println!("{}", value),
                impass::database::FieldValue::Binary(value) => {
                    // NOTE: Newline is intentionally not added for binary data to avoid the need
                    // for users to chomp it.
                    std::io::stdout().write_all(&value)?
                }
            }
        }
    } else {
        for (name, field) in {
            let mut fields = entry.fields().iter().collect::<Vec<_>>();
            fields.sort_unstable_by_key(|(name, _)| *name);
            fields
        } {
            println!(
                "{} ({}) - {}",
                name,
                field.mime().essence_str(),
                match field.value()? {
                    impass::database::FieldValue::Utf8(value) => value,
                    impass::database::FieldValue::Binary(_) => "<binary string>",
                }
            )
        }
    }

    Ok(())
}


fn add(database: &mut impass::Database, matches: &clap::ArgMatches) -> anyhow::Result<()> {
    fn insert<F: Fn(&str) -> anyhow::Result<Vec<u8>>>(
        builder: &mut impass::EntryBuilder,
        field: Vec<&str>,
        parse: F,
        mime: impass::mime::Mime,
    ) -> anyhow::Result<()> {
        let name = field[0];
        let value = parse(field[1])?;
        let mime = if field.len() > 2 && field[2].is_empty() {
            field[2].parse()?
        } else {
            mime
        };

        builder.insert(
            name,
            if field.len() > 3 {
                let hints = {
                    let mut hints = std::collections::HashSet::new();
                    for hint in &field[3..] {
                        hints.insert(hint.parse()?);
                    }
                    hints
                };

                impass::Field::with_hints(mime, value, hints)
            } else {
                impass::Field::new(mime, value)
            }?,
        );

        Ok(())
    }

    let fields = partition(matches, "field");
    let files = partition(matches, "file");
    let paths = partition(matches, "path");

    let mut builder = impass::EntryBuilder::default();
    for field in fields {
        insert(
            &mut builder,
            field,
            |v| Ok(v.to_owned().into_bytes()),
            impass::mime::TEXT_PLAIN_UTF_8,
        )?;
    }
    for file in files {
        insert(
            &mut builder,
            file,
            |v| std::fs::read(v).map_err(anyhow::Error::from),
            impass::mime::APPLICATION_OCTET_STREAM,
        )?;
    }
    let id = builder.store(database);

    for path in paths {
        let (group, name) = path.split_at(path.len() - 1);
        let name = name[0];
        if matches.is_present("create") {
            database.hierarchy.insert(&group, name, id)
        } else {
            database
                .hierarchy
                .at_mut(&group)
                .ok_or_else(|| impass::database::GroupDoesNotExist::from(&group[..]))?
                .insert(&[], name, id)
        }?
    }

    Ok(())
}


/// Partition an argument that allows multiple occurrences of multiple values.
fn partition<'a>(matches: &'a clap::ArgMatches<'a>, name: &str) -> Vec<Vec<&'a str>> {
    let mut items = match matches
        .indices_of(name)
        .map(|iterator| iterator.zip(matches.values_of(name).unwrap()).peekable())
    {
        Some(iterator) => iterator,
        None => return vec![],
    };

    if let Some((first, _)) = items.peek() {
        let mut previous = first - 1;
        let mut partitioned = vec![vec![]];

        for (index, item) in items {
            match (index - previous).cmp(&1) {
                std::cmp::Ordering::Greater => partitioned.push(vec![]),
                std::cmp::Ordering::Less => panic!("unexpected order of indices"),
                _ => {}
            }

            partitioned.last_mut().unwrap().push(item);
            previous = index;
        }

        if matches.occurrences_of(name) == partitioned.len() as u64 + 1 {
            partitioned.push(vec![]);
        } else if matches.occurrences_of(name) != partitioned.len() as u64 {
            panic!("unexpected number of ocurrences")
        }

        partitioned
    } else {
        vec![]
    }
}
