use block_modes::BlockMode;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_json as json;
use sha2::Digest;
use std::collections::HashMap;
use std::convert::TryInto;
use thiserror::Error;


#[derive(Debug, Error)]
pub enum DecryptionError {
    #[error(transparent)]
    Base64(#[from] base64::DecodeError),
    #[error("either the encrypted buffer was not a multiple of the block size, or there was malformed padding")]
    BlockMode(#[from] block_modes::BlockModeError),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    Json(#[from] json::Error),
    #[error(transparent)]
    WrongSize(#[from] WrongSize),
}

#[derive(Debug, Error)]
#[error("expected {expected}-byte {kind:?} but got {actual}-byte key")]
pub struct WrongSize {
    actual: usize,
    expected: usize,
    kind: WrongSizeKind,
}

#[derive(Debug)]
pub enum WrongSizeKind {
    Key,
    Vector,
}

#[derive(Debug, Error)]
pub enum LoadError {
    #[error(transparent)]
    Base64(#[from] base64::DecodeError),
    #[error(transparent)]
    Integrity(#[from] IntegrityError),
    #[error(transparent)]
    Json(#[from] json::Error),
}

/// Database file failed integrity checks.
#[derive(Debug, Error)]
pub struct IntegrityError {
    expected: Vec<u8>,
    actual: Vec<u8>,
}

#[derive(Debug, Error)]
pub enum EncryptionError {
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    Json(#[from] json::Error),
    #[error(transparent)]
    WrongSize(#[from] WrongSize),
}

impl std::fmt::Display for IntegrityError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        fn hex(bytes: &[u8]) -> String {
            bytes
                .iter()
                .map(|b| format!("{:02x}", b))
                .collect::<Vec<_>>()
                .join("")
        }

        write!(
            f,
            "database failed integrity checks\n\texpected: {}\n\t     got: {}",
            hex(&self.expected),
            hex(&self.actual)
        )
    }
}


// TODO: Manually implement Deserialize so that unsupported versions are errors.
/// The version of this database file.
///
/// Two files that have the same major version will be _backwards_ compatible.
/// This means that a given file with minor version `x` will successfully
/// validate against the schemas for all minor versions `x+n` (where `n` is
/// positive) as long as the major version is the same.
///
/// No guarantees are made about the compatibility of different major versions.
///
/// Applications should always check this to ensure that they work correctly for
/// this file's version.
#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, Ord, PartialEq, PartialOrd, Serialize)]
pub struct Version {
    pub major: u64,
    pub minor: u64,
}

// TODO: Make Compression a trait object, _if_ serde and schemars handle it
// well.
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
enum Compression {
    Gzip,
}

impl Default for Compression {
    fn default() -> Self {
        Compression::Gzip
    }
}

impl Compression {
    fn compress(&self, data: &[u8]) -> Result<Vec<u8>, std::io::Error> {
        match self {
            Compression::Gzip => self.compress_gzip(data),
        }
    }

    fn compress_gzip(&self, mut data: &[u8]) -> Result<Vec<u8>, std::io::Error> {
        let mut encoder = libflate::gzip::Encoder::new(Vec::new())?;
        std::io::copy(&mut data, &mut encoder)?;
        encoder.finish().into_result()
    }

    fn decompress(&self, data: &[u8]) -> Result<Vec<u8>, std::io::Error> {
        match self {
            Compression::Gzip => self.decompress_gzip(data),
        }
    }

    fn decompress_gzip(&self, data: &[u8]) -> Result<Vec<u8>, std::io::Error> {
        let mut decoder = libflate::gzip::Decoder::new(data)?;
        let mut uncompressed = Vec::new();
        std::io::copy(&mut decoder, &mut uncompressed)?;
        Ok(uncompressed)
    }
}

// TODO: This should use actual structs so that we can impl transform, decrypt,
// and encrypt on them. Or maybe it should be a trait object instead of an enum.
/// Encryption algorithm used.
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
enum Algorithm {
    Aes256 { seed: [u8; 32], rounds: u64 },
}

impl Algorithm {
    fn transform(&self, key: &[u8]) -> Vec<u8> {
        match self {
            Algorithm::Aes256 { seed, rounds } => self
                .transform_aes256(
                    sha2::Sha256::digest(&key)
                        .as_slice()
                        .try_into()
                        .expect("SHA-256 should always produce 256-bit keys"),
                    *seed,
                    *rounds,
                )
                .to_vec(),
        }
    }

    fn transform_aes256(&self, mut key: [u8; 32], seed: [u8; 32], rounds: u64) -> [u8; 32] {
        type Aes256Ecb = block_modes::Ecb<aes::Aes256, block_modes::block_padding::ZeroPadding>;

        for _ in 0..rounds {
            let cipher = Aes256Ecb::new_var(&seed, &[])
                .expect("length of initialisation vector is known at compile time");

            cipher
                .encrypt(&mut key, 32)
                .expect("length of key is known at compile time");
        }

        sha2::Sha256::digest(&key)
            .as_slice()
            .try_into()
            .expect("SHA-256 should always produce 256 bit keys")
    }

    fn decrypt(
        &self,
        encrypted: &[u8],
        key: &[u8],
        vector: &[u8],
    ) -> Result<Vec<u8>, DecryptionError> {
        match self {
            Algorithm::Aes256 { .. } => {
                let key = key.try_into().map_err(|_| {
                    DecryptionError::WrongSize(WrongSize {
                        actual: key.len(),
                        expected: 32,
                        kind: WrongSizeKind::Key,
                    })
                })?;

                let vector = vector.try_into().map_err(|_| {
                    DecryptionError::WrongSize(WrongSize {
                        actual: vector.len(),
                        expected: 32,
                        kind: WrongSizeKind::Vector,
                    })
                })?;

                self.decrypt_aes256(encrypted, key, vector)
                    .map_err(DecryptionError::from)
            }
        }
    }

    fn decrypt_aes256(
        &self,
        encrypted: &[u8],
        key: [u8; 32],
        vector: [u8; 16],
    ) -> Result<Vec<u8>, block_modes::BlockModeError> {
        type Aes256Pcbc = block_modes::Pcbc<aes::Aes256, block_modes::block_padding::Pkcs7>;

        let cipher = Aes256Pcbc::new_var(&key, &vector)
            .expect("lengths of key and initialisation vector are known at compile time");

        cipher.decrypt_vec(encrypted)
    }

    fn encrypt(&self, data: &[u8], key: &[u8], vector: &[u8]) -> Result<Vec<u8>, WrongSize> {
        match self {
            Algorithm::Aes256 { .. } => {
                let key = key.try_into().map_err(|_| WrongSize {
                    actual: key.len(),
                    expected: 32,
                    kind: WrongSizeKind::Key,
                })?;

                let vector = vector.try_into().map_err(|_| WrongSize {
                    actual: vector.len(),
                    expected: 32,
                    kind: WrongSizeKind::Vector,
                })?;

                Ok(self.encrypt_aes256(data, key, vector))
            }
        }
    }

    fn encrypt_aes256(&self, data: &[u8], key: [u8; 32], vector: [u8; 16]) -> Vec<u8> {
        type Aes256Pcbc = block_modes::Pcbc<aes::Aes256, block_modes::block_padding::Pkcs7>;

        let cipher = Aes256Pcbc::new_var(&key, &vector)
            .expect("lengths of key and initialisation vector are known at compile time");

        cipher.encrypt_vec(data)
    }
}

/// Parameters for the encryption used for this file.
///
/// Encryption used in ImPass is strongly based on the encryption used by
/// [KeePass][kp_encryption].
///
/// [kp_encryption]: https://keepass.info/help/base/security.html
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
struct Encryption {
    /// Algorithm used for encrypting this file.
    algorithm: Algorithm,
    /// Master seed used for all algorithms.
    seed: [u8; 32],
    /// Initialisation vector that is randomly generated each save.
    vector: [u8; 16],
}

impl Encryption {
    fn key(&self, password: &[u8]) -> [u8; 32] {
        sha2::Sha256::digest(
            self.seed
                .iter()
                .copied()
                .chain(self.algorithm.transform(password).into_iter())
                .collect::<Vec<_>>()
                .as_slice(),
        )
        .as_slice()
        .try_into()
        .expect("SHA-256 should always produce 256-bit keys")
    }
}

/// Hash used for integrity checking this file.
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
enum Hash {
    Sha256([u8; 32]),
}

/// Metadata about this file.
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
pub struct Metadata {
    /// Application-defined metadata about this file.
    ///
    /// Applications should take care to make sure they do not use names that
    /// are too generic.
    pub extra: HashMap<String, json::Value>,
    pub version: Version,
    compression: Compression,
    encryption: Encryption,
    /// Base64 encoded hash of the encrypted DB for integrity checking.
    hash: Hash,
}

impl Metadata {
    fn persist(self) -> PersistentMetadata {
        PersistentMetadata {
            extra: self.extra,
            version: Version {
                major: self.version.major,
                minor: self.version.minor,
            },
            compression: self.compression,
            encryption: PersistentEncryption {
                algorithm: match self.encryption.algorithm {
                    Algorithm::Aes256 { seed, rounds } => Algorithm::Aes256 { seed, rounds },
                },
                seed: self.encryption.seed,
            },
            hash: std::mem::discriminant(&self.hash),
        }
    }
}

/// Metadata where the necessary encryption parameters have been renewed.
#[derive(Debug)]
pub struct RenewedMetadata {
    pub extra: HashMap<String, json::Value>,
    pub version: Version,
    compression: Compression,
    encryption: Encryption,
    hash: std::mem::Discriminant<Hash>,
}

impl RenewedMetadata {
    pub fn hash(self, contents: &[u8]) -> Metadata {
        Metadata {
            extra: self.extra,
            version: Version {
                major: self.version.major,
                minor: self.version.minor,
            },
            compression: self.compression,
            encryption: Encryption {
                algorithm: match self.encryption.algorithm {
                    Algorithm::Aes256 { seed, rounds } => Algorithm::Aes256 { seed, rounds },
                },
                seed: self.encryption.seed,
                vector: self.encryption.vector,
            },
            hash: if self.hash == std::mem::discriminant(&Hash::Sha256([0; 32])) {
                Hash::Sha256(
                    sha2::Sha256::digest(contents)
                        .as_slice()
                        .try_into()
                        .expect("SHA-256 should always produce 256-bit keys"),
                )
            } else {
                unreachable!()
            },
        }
    }
}

#[derive(Debug)]
struct PersistentEncryption {
    algorithm: Algorithm,
    seed: [u8; 32],
}

/// Metadata that does not need to be reset every time the database changes.
#[derive(Debug)]
pub struct PersistentMetadata {
    pub extra: HashMap<String, json::Value>,
    pub version: Version,
    compression: Compression,
    encryption: PersistentEncryption,
    hash: std::mem::Discriminant<Hash>,
}

impl Default for PersistentMetadata {
    fn default() -> Self {
        PersistentMetadata {
            extra: HashMap::new(),
            version: Version { major: 0, minor: 0 },
            compression: Default::default(),
            encryption: PersistentEncryption {
                algorithm: Algorithm::Aes256 {
                    seed: rand::random(),
                    rounds: 50_000,
                },
                seed: rand::random(),
            },
            hash: std::mem::discriminant(&Hash::Sha256([0; 32])),
        }
    }
}

impl PersistentMetadata {
    pub fn with_aes256(mut self, rounds: u64) -> Self {
        self.encryption.algorithm = Algorithm::Aes256 {
            seed: rand::random(),
            rounds,
        };
        self
    }

    pub fn renew(self) -> RenewedMetadata {
        RenewedMetadata {
            extra: self.extra,
            version: Version {
                major: self.version.major,
                minor: self.version.minor,
            },
            compression: self.compression,
            encryption: Encryption {
                algorithm: match self.encryption.algorithm {
                    Algorithm::Aes256 { seed, rounds } => Algorithm::Aes256 { seed, rounds },
                },
                seed: self.encryption.seed,
                vector: rand::random(),
            },
            hash: self.hash,
        }
    }
}

/// The database file before decryption.
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
pub struct Encrypted {
    pub metadata: Metadata,
    // TODO: Add validation that this is base64 once https://github.com/GREsau/schemars/issues/12 is done.
    /// The database structure; compressed, encrypted, then base64 encoded.
    database: String,
}

impl Encrypted {
    pub fn load<R: std::io::Read>(reader: R) -> Result<Encrypted, LoadError> {
        let encrypted: Encrypted = json::from_reader(reader).map_err(LoadError::from)?;

        let (expected, actual) = match encrypted.metadata.hash {
            Hash::Sha256(ref expected) => (
                expected.to_vec(),
                sha2::Sha256::digest(encrypted.database.as_bytes())
                    .as_slice()
                    .to_vec(),
            ),
        };

        if expected != actual {
            Err(LoadError::Integrity(IntegrityError { expected, actual }))
        } else {
            Ok(encrypted)
        }
    }

    pub fn save<W: std::io::Write>(&self, writer: W) -> Result<(), json::Error> {
        json::to_writer(writer, self)
    }

    pub fn encrypt(
        metadata: PersistentMetadata,
        database: super::database::Database,
        password: &[u8],
    ) -> Result<Encrypted, EncryptionError> {
        Decrypted { metadata, database }.encrypt(password)
    }

    pub fn decrypt(self, password: &[u8]) -> Result<Decrypted, DecryptionError> {
        let Encrypted {
            metadata,
            database: base64,
        } = self;

        let string = base64::decode(&base64)?;
        let key = metadata.encryption.key(password);

        let decrypted =
            metadata
                .encryption
                .algorithm
                .decrypt(&string, &key, &metadata.encryption.vector)?;

        let uncompressed = metadata.compression.decompress(&decrypted)?;

        let database = json::from_slice(&uncompressed)?;

        Ok(Decrypted {
            metadata: metadata.persist(),
            database,
        })
    }
}

#[derive(Debug)]
pub struct Decrypted {
    pub metadata: PersistentMetadata,
    pub database: super::database::Database,
}

impl Decrypted {
    pub fn encrypt(self, password: &[u8]) -> Result<Encrypted, EncryptionError> {
        let Decrypted { metadata, database } = self;

        let string = json::to_string(&database)?;
        let metadata = metadata.renew();
        let key = metadata.encryption.key(password);

        let compressed = metadata.compression.compress(string.as_bytes())?;

        let encrypted = metadata.encryption.algorithm.encrypt(
            &compressed,
            &key,
            &metadata.encryption.vector,
        )?;

        let base64 = base64::encode(&encrypted);

        Ok(Encrypted {
            metadata: metadata.hash(base64.as_bytes()),
            database: base64,
        })
    }
}
