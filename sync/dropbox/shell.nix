let pkgs = import <nixpkgs> { };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    cargo
    clippy
    openssl
    pkg-config
    rls
    rustc
    rustfmt
  ];
}
