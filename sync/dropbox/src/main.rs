use clap::{crate_authors, crate_description, crate_name, crate_version};
use serde_json as json;


#[derive(Debug, serde::Serialize)]
struct StartRequest {
    close: bool,
}

#[derive(Debug, serde::Serialize)]
struct Cursor<'s> {
    offset: usize,
    session_id: &'s str,
}

#[derive(Debug, serde::Serialize)]
struct AppendRequest<'s> {
    close: bool,
    cursor: Cursor<'s>,
}

#[derive(Debug, serde::Serialize)]
struct Commit<'p> {
    autorename: bool,
    mode: &'static str,
    mute: bool,
    path: &'p str,
    strict_conflict: bool,
}

#[derive(Debug, serde::Serialize)]
struct FinishRequest<'p, 's> {
    commit: Commit<'p>,
    cursor: Cursor<'s>,
}


#[allow(clippy::cognitive_complexity)]
fn main() -> anyhow::Result<()> {
    let matches = clap::app_from_crate!()
        .help_message("Print help information.")
        .version_message("Print version information.")
        .arg(
            clap::Arg::with_name("verbose")
                .long("verbose")
                .short("v")
                .help("Enable debug logging."),
        )
        .arg(
            clap::Arg::with_name("file")
                .long("file")
                .short("f")
                .help("Path to the database.")
                .takes_value(true)
                .required(true),
        )
        .arg(
            clap::Arg::with_name("token")
                .long("token")
                .short("t")
                .help("Authorization token for Dropbox.")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let level = if matches.occurrences_of("verbose") > 1 {
        simplelog::LevelFilter::Trace
    } else if cfg!(debug_assertions) || matches.is_present("verbose") {
        simplelog::LevelFilter::Debug
    } else {
        simplelog::LevelFilter::Info
    };

    if simplelog::TermLogger::init(
        level,
        simplelog::Config::default(),
        simplelog::TerminalMode::Stderr,
    )
    .is_err()
    {
        simplelog::SimpleLogger::init(level, simplelog::Config::default()).unwrap();
    }

    let path_str = matches.value_of("file").unwrap();
    log::trace!("Original path: {:?}", path_str);

    let path = std::path::Path::new(path_str).canonicalize()?;
    log::trace!("Canonical path: {:?}", path);

    let directory = path.parent().unwrap();
    log::debug!("Watching directory: {:?}", directory);

    let name_to_watch = path.file_name().unwrap().to_owned();
    log::debug!("Watching file: {:?}", name_to_watch);

    let upload_path = {
        let mut upload_path = std::ffi::OsString::from("/");
        upload_path.push(&name_to_watch);
        upload_path
    };
    let upload_path = upload_path.to_str().unwrap();
    log::debug!("Upload path: {:?}", upload_path);


    let mut inotify = inotify::Inotify::init()?;
    inotify.add_watch(
        &directory,
        inotify::WatchMask::CLOSE_WRITE | inotify::WatchMask::MOVED_TO,
    )?;

    let (sender, receiver) = std::sync::mpsc::channel();
    // Always do an upload when the application starts.
    sender.send(()).unwrap();
    std::thread::spawn(move || loop {
        log::trace!("Reading events...");

        let mut buffer = [0; 1024];
        for event in inotify.read_events_blocking(&mut buffer).unwrap() {
            log::debug!(
                "Received event: {:010} - {:?} - {:?}",
                event.cookie,
                event.mask,
                event.name
            );

            let inotify::Event {
                cookie, mask, name, ..
            } = event;
            let name = name.unwrap();

            if name == name_to_watch {
                if mask.intersects(inotify::EventMask::CLOSE_WRITE | inotify::EventMask::MOVED_TO) {
                    sender.send(()).unwrap();
                } else {
                    log::warn!("Unknown event: {:010} - {:?} - {:?}", cookie, mask, name);
                }
            } else {
                log::info!("Ignoring file: {:?}", name);
            }
        }
    });


    let token = matches.value_of("token").unwrap();
    log::debug!("Dropbox API token: {:?}", token);

    let octet = "application/octet-stream";
    let arg = "dropbox-api-arg";

    let mut database: Option<impass::Encrypted> = None;
    loop {
        log::trace!("Waiting for file change...");
        receiver.recv()?;

        log::trace!("Ignoring duplicate messages...");
        while let Ok(_) = receiver.try_recv() {
            log::trace!("Message ignored.");
        }

        log::info!("Checking database...");

        log::trace!("Opening file...");
        let file = std::fs::File::open(&path)?;

        log::trace!("Deserializing file...");
        let new = json::from_reader::<_, impass::Encrypted>(&file);
        database = match new {
            Ok(new)
                if database
                    .as_ref()
                    .map(|database| database != &new)
                    .unwrap_or(true) =>
            {
                log::info!("Database changed.");
                Some(new)
            }
            Ok(_) => {
                log::info!("Database unchanged.");
                continue;
            }
            Err(error) => {
                if error.is_io() {
                    log::error!("IO error: {}", error);
                    return Err(anyhow::Error::from(error));
                } else {
                    log::warn!("Invalid database: {}", error);
                    continue;
                }
            }
        };

        log::info!("Uploading file...");

        let data = json::to_vec(database.as_ref().unwrap())?;
        let client = reqwest::blocking::Client::new();

        log::trace!("Starting upload...");

        let response = client
            .post("https://content.dropboxapi.com/2/files/upload_session/start")
            .header(reqwest::header::CONTENT_TYPE, octet)
            .header(arg, json::to_vec(&StartRequest { close: false })?)
            .bearer_auth(token)
            .send()?
            .text()?;

        let session_id = match json::from_str::<json::Value>(&response) {
            Err(error) => {
                log::error!(
                    "Response was not valid JSON.\nResponse: {}\nError: {}",
                    response,
                    error,
                );
                continue;
            }
            Ok(response) => {
                log::trace!("Received response: {:?}", response);
                match response {
                    json::Value::Object(mut object) => {
                        if let Some(json::Value::String(session_id)) = object.remove("session_id") {
                            session_id
                        } else {
                            log::error!("Invalid response object: {:?}", object);
                            continue;
                        }
                    }
                    other => {
                        log::error!("Invalid response: {:?}", other);
                        continue;
                    }
                }
            }
        };
        log::debug!("Session ID: {}", session_id);

        let mut offset = 0;
        for (i, chunk) in data.chunks(150_000_000).enumerate() {
            let size = chunk.len();

            log::trace!("Uploading chunk: {} - {} bytes", i, size);

            let response = client
                .post("https://content.dropboxapi.com/2/files/upload_session/append_v2")
                .header(reqwest::header::CONTENT_TYPE, octet)
                .header(
                    arg,
                    json::to_vec(&AppendRequest {
                        close: false,
                        cursor: Cursor { offset, session_id: session_id.as_str() },
                    })?,
                )
                .bearer_auth(token)
                // TODO: Is this allocation really necessary?
                .body(chunk.to_vec())
                .send()?
                .text()?;

            match json::from_str::<json::Value>(&response) {
                Err(error) => {
                    log::error!(
                        "Response was not valid JSON.\nResponse: {}\nError: {}",
                        response,
                        error
                    );
                    break;
                }
                Ok(response) => {
                    log::trace!("Received response: {:?}", response);

                    match response {
                        json::Value::Null => {}
                        json::Value::Object(mut object) => {
                            if let Some(json::Value::String(error)) = object.remove("error_summary")
                            {
                                log::error!("Error uploading chunk: {} - {}", i, error);
                            } else {
                                log::error!("Invalid response object: {:?}", object);
                            }
                            break;
                        }
                        other => {
                            log::error!("Invalid response: {:?}", other);
                            break;
                        }
                    }
                }
            }

            offset += size;
        }
        log::debug!("Uploaded {} bytes.", offset);

        log::trace!("Finishing upload...");
        let response = client
            .post("https://content.dropboxapi.com/2/files/upload_session/finish")
            .header(reqwest::header::CONTENT_TYPE, octet)
            .header(
                arg,
                json::to_vec(&FinishRequest {
                    commit: Commit {
                        autorename: false,
                        mode: "overwrite",
                        mute: true,
                        path: upload_path,
                        strict_conflict: true,
                    },
                    cursor: Cursor {
                        offset,
                        session_id: session_id.as_str(),
                    },
                })?,
            )
            .bearer_auth(token)
            .send()?
            .text()?;

        match json::from_str::<json::Value>(&response) {
            Err(error) => log::error!(
                "Response was not valid JSON.\nResponse: {}\nError: {}",
                response,
                error
            ),
            Ok(response) => {
                log::trace!("Received response: {:?}", response);

                match response {
                    json::Value::Object(mut object) => {
                        if let Some(json::Value::String(error)) = object.remove("error_summary") {
                            log::error!("Error finishing upload: {}", error);
                        } else {
                            log::info!("Upload successful.");
                        }
                    }
                    other => log::error!("Invalid response: {:?}", other),
                }
            }
        }
    }
}
