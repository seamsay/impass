use clap::{crate_authors, crate_description, crate_name, crate_version};
use futures::StreamExt;
use serde_json as json;
use tokio::io::{AsyncReadExt, AsyncWriteExt};


async fn notifier(
    mut database: impass::Encrypted,
    path: std::path::PathBuf,
    sender: tokio::sync::watch::Sender<impass::Encrypted>,
) -> anyhow::Result<()> {
    let path = path.canonicalize().unwrap();
    let directory = path.parent().unwrap();
    let file = path.file_name().unwrap();

    log::debug!("Watching {:?} for {:?}.", directory, file);

    let mut inotify = inotify::Inotify::init()?;
    inotify.add_watch(
        &directory,
        inotify::WatchMask::CLOSE_WRITE | inotify::WatchMask::MOVED_TO,
    )?;

    let mut buffer = [0; 32];
    let mut stream = inotify.event_stream(&mut buffer)?;
    while let Some(event) = stream.next().await {
        let event = event?;
        log::trace!(
            "Received event: {:010} - {:?} - {:?}",
            event.cookie,
            event.mask,
            event.name
        );

        let inotify::Event {
            cookie, mask, name, ..
        } = event;
        let name = name.unwrap();
        let name = name.as_os_str();

        if name == file {
            if mask.intersects(inotify::EventMask::CLOSE_WRITE | inotify::EventMask::MOVED_TO) {
                let new = tokio::fs::read(&path).await?;
                match json::from_slice::<impass::Encrypted>(&new) {
                    Ok(new) if new != database => {
                        log::info!("Database changed.");
                        sender.broadcast(new.clone())?;
                        database = new;
                    }
                    Ok(_) => log::debug!("Database unchanged."),
                    Err(error) => log::warn!("Database could not be deserialized: {}", error),
                }
            } else {
                log::warn!("Unknown event: {:010} - {:?} - {:?}", cookie, mask, name,);
            }
        } else {
            log::info!("Ignoring changed file: {:?}", name);
        }
    }

    Ok(())
}


async fn server(
    notifier: tokio::sync::watch::Receiver<impass::Encrypted>,
    path: std::path::PathBuf,
    address: String,
) -> anyhow::Result<()> {
    let mut listener = tokio::net::TcpListener::bind(address).await?;
    while let Some(stream) = listener.next().await {
        match stream {
            Err(error) => log::error!("Failed to accept connection: {}", error),
            Ok(stream) => {
                log::info!("Accepted connection: {}", stream.peer_addr().unwrap());
                let notifier = notifier.clone();
                let path = path.clone();
                tokio::spawn(async move { handler(notifier, stream, path).await });
            }
        }
    }

    Ok(())
}


#[allow(clippy::cognitive_complexity)]
async fn handler(
    mut notifier: tokio::sync::watch::Receiver<impass::Encrypted>,
    mut stream: tokio::net::TcpStream,
    path: std::path::PathBuf,
) {
    #[derive(Debug, Eq, PartialEq, serde::Deserialize, serde::Serialize)]
    struct Version {
        major: u64,
        minor: u64,
    }

    #[derive(Debug, serde::Deserialize, serde::Serialize)]
    // Value is only ever short-lived.
    #[allow(clippy::large_enum_variant)]
    enum Receive {
        Handshake(Version),
        Change(impass::Encrypted),
        Heartbeat,
    }

    #[derive(Debug, serde::Deserialize, serde::Serialize)]
    enum Error {
        Deserialization(String),
        MultipleHandshakes,
        NoHandshake,
        VersionNotSupported {
            received: Version,
            supported: Vec<Version>,
        },
    }

    #[derive(Debug, serde::Deserialize, serde::Serialize)]
    // Value is only ever short-lived.
    #[allow(clippy::large_enum_variant)]
    enum Respond {
        Handshake(impass::Encrypted),
        Change(impass::Encrypted),
        Heartbeat,
        Error(Error),
    }

    // TODO: When can this happen, should it be handled gracefully?
    let client = stream.peer_addr().unwrap();

    macro_rules! r#return {
        ($level:ident, $message:literal, $($args:tt),*) => {
            log::log!(log::Level::$level, concat!("{client}: ", $message), $($args,)* client=client);
            return;
        };
    }

    // UNWRAP: If the notifier died the application can't work.
    let database = notifier.recv().await.unwrap();

    let buffer = {
        let mut buffer = vec![];
        if let Err(error) = stream.read_buf(&mut buffer).await {
            r#return!(Error, "Failed to read handshake: {}", error);
        }
        buffer
    };

    match json::from_slice(&buffer) {
        Ok(Receive::Handshake(version)) if version == Version { major: 0, minor: 0 } => {
            if let Err(error) = stream
                // UNWRAP: Value is statically guaranteed to be serializable.
                .write_all(&json::to_vec(&Respond::Handshake(database)).unwrap())
                .await
            {
                r#return!(Error, "Failed to respond to handshake: {}", error);
            } else {
                log::debug!("{}: Handshake successful.", client);
            }
        }
        otherwise => {
            let error = match otherwise {
                Ok(Receive::Handshake(version)) => Error::VersionNotSupported {
                    received: version,
                    supported: vec![Version { major: 0, minor: 0 }],
                },
                Ok(Receive::Change(_)) | Ok(Receive::Heartbeat) => Error::NoHandshake,
                Err(error) => Error::Deserialization(format!("{}", error)),
            };

            // UNWRAP: Value is statically guaranteed to be serializable.
            if let Err(error) = stream.write_all(&json::to_vec(&error).unwrap()).await {
                r#return!(Error, "Failed to send handshake error: {}", error);
            }

            r#return!(Error, "Handshake failed: {:?}", error);
        }
    }

    let (mut reader, mut writer) = stream.split();

    loop {
        let mut buffer = vec![];

        tokio::select! {
            local = notifier.recv() => {
                // UNWRAP: If notifier goes down, the application can't work.
                let local = local.unwrap();
                if let Err(error) = writer
                    // UNWRAP: Value is statically guaranteed to be serializable.
                    .write_all(&json::to_vec(&Respond::Change(local)).unwrap())
                    .await
                {
                    r#return!(Error, "Failed to send change to client: {}", error);
                }
            }
            _ = reader.read_buf(&mut buffer) => {
                match json::from_slice(&buffer) {
                    Ok(Receive::Heartbeat) => {
                        log::info!("{client}: Heartbeat received.", client = client)
                    }
                    Ok(Receive::Change(remote)) => {
                        // UNWRAP: Value is statically guaranteed to be serializable.
                        if let Err(error) =
                            tokio::fs::write(&path, json::to_vec(&remote).unwrap()).await
                        {
                            log::warn!(
                                "{client}: Failed to write database to disk: {}",
                                error,
                                client = client
                            );
                        }
                    }
                    Ok(Receive::Handshake(_)) => {
                        if let Err(error) = writer
                            .write_all(
                                // UNWRAP: Value is statically guaranteed to be serializable.
                                &json::to_vec(&Respond::Error(Error::MultipleHandshakes)).unwrap(),
                            )
                            .await
                        {
                            r#return!(
                                Error,
                                "Failed to send multiple handshake error to client: {}",
                                error
                            );
                        }
                    }
                    Err(deserialize) => {
                        if let Err(send) = writer
                            .write_all(
                                &json::to_vec(&Respond::Error(Error::Deserialization(format!(
                                    "{}",
                                    deserialize
                                ))))
                                // UNWRAP: Value is statically guaranteed to be serializable.
                                .unwrap(),
                            )
                            .await
                        {
                            r#return!(
                                Error,
                                "Failed to send deserialization error to client: {}",
                                send
                            );
                        }
                    }
                }
            }
        }
    }
}


#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let matches = clap::app_from_crate!()
        .help_message("Print help information.")
        .version_message("Print version information.")
        .arg(
            clap::Arg::with_name("verbose")
                .long("verbose")
                .short("d")
                .help("Enable debug logging."),
        )
        .arg(
            clap::Arg::with_name("file")
                .long("file")
                .short("f")
                .help("Path to the database.")
                .takes_value(true)
                .required(true),
        )
        .arg(
            clap::Arg::with_name("ip")
                .long("ip")
                .short("i")
                .help("Listen on this IP address.")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("port")
                .long("port")
                .short("p")
                .help("Port to listen on.")
                .takes_value(true)
                .default_value("1112"),
        )
        .get_matches();

    let level = if cfg!(debug_assertions) || matches.is_present("verbose") {
        simplelog::LevelFilter::Trace
    } else {
        simplelog::LevelFilter::Info
    };

    if simplelog::TermLogger::init(
        level,
        simplelog::Config::default(),
        simplelog::TerminalMode::Stderr,
    )
    .is_err()
    {
        simplelog::SimpleLogger::init(level, simplelog::Config::default()).unwrap();
    }

    let path = matches.value_of("file").unwrap().to_owned();
    let address = {
        let ip = if let Some(ip) = matches.value_of("ip") {
            ip
        } else if cfg!(debug_assertions) {
            "localhost"
        } else {
            "0.0.0.0"
        };
        let port = matches.value_of("port").unwrap();

        format!("{}:{}", ip, port)
    };

    log::debug!("Opening database `{}`...", path);
    let database: impass::Encrypted = json::from_slice(&tokio::fs::read(&path).await?)?;

    log::trace!("Spawning file watcher...");
    let (notifier_sender, notifier_receiver) = tokio::sync::watch::channel(database.clone());
    let notifier = {
        let path = (&path).into();
        tokio::spawn(async move { notifier(database, path, notifier_sender).await })
    };

    log::trace!("Spawning server...");
    let server = tokio::spawn(async move { server(notifier_receiver, path.into(), address).await });

    let (notifier, server) = tokio::try_join!(notifier, server)?;
    notifier.and(server)
}
